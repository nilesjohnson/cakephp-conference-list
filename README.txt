Cake PHP Conference List
https://bitbucket.org/nilesjohnson/cakephp-conference-list

version 1.0
July 2012

Copyright (C) 2009--2012 Niles Johnson <http://www.nilesjohnson.net>
Licensed under GNU GPL v.3 or later.  See LICENSE.txt for a copy of
the license.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


DESCRIPTION
-----------

Cake PHP Conference List is a web application for community-maintained
public lists (e.g. math conferences).  Its basic functions are:

* A web form for adding new announcements, storing them in a sqlite database.
* An interface for viewing announcements, sorted by date or location.
* Interfaces to update and delete announcements.

The application is based on the Cake PHP framework (version 1.3.2).


CONFIGURATION
-------------

You will need an installation of Cake PHP for this application.  Here
is one reasonable setup:

   Cake core in a directory <CAKEPHP>
   This application in a directory <CONF-LIST>, so the 'app' directory is at
   <CONF-LIST>/app

The configuration consists of two necessary steps and one optional step:

First, you will need to edit the following for your installation, changing
"/path/to/cakephp-conference-list" to your <CONF-LIST> directory in
each one:
   
   <CONF-LIST>/.htaccess
   <CONF-LIST>/app/.htaccess
   <CONF-LIST>/app/webroot/.htaccess
    
Next, you will need to edit <CONF-LIST>/app/webroot/index.php as
described in the "Advanced Installation section of the Cake Book".
This consists of setting the variables ROOT, APP_DIR, and
CAKE_CORE_INCLUDE_PATH to point to the relevant directories.  Since
this application is in <CONF-LIST>/app, you should just need to edit
ROOT to point to <CONF-LIST> and CAKE-CORE-INCLUDE-PATH to point to
<CAKEPHP>.

Lastly (optional), you can change the name of the sqlite database
file.  To do this, rename or replace <CONF-LIST>/app/webroot/db.sqlite
(keeping the database in this directory).  Then edit
<CONF-LIST>/app/config/database.php to point to your database.  The
recommended format is sqlite, and to use this you just need to change
the database 'db.sqlite' in the $default array of DATABASE_CONFIG to
the name of your database in <CONF-LIST>/app/webroot.


ADDITIONAL CUSTOMIZATION
------------------------

A number of other internal variables and details will need to be set 
according to your applicaiton.  Here is a list of some:

 * Email addresses in app/controllers/components/email_key.php
 * Contact urls in app/views/layouts/default.ctp



