<?php 
 Print "Hello, World!";
 ?> 

<?php

// create new database (OO interface)
$db = new SQLiteDatabase("cake_1.3.2/app/webroot/db.sqlite");


// uncomment the following to reset the conferences table
/*
$db->query("BEGIN;
DROP TABLE conferences;
COMMIT;");
*/

// uncomment this to create the conferences table,
// and some dummy entries
/*

$db->query("BEGIN;
CREATE TABLE conferences (
id INTEGER PRIMARY KEY,
edit_key TEXT,
title TEXT,
start_date TEXT,
end_date TEXT,
duration NUMERIC DEFAULT NULL,
city TEXT,
region TEXT,
meeting_type TEXT,
subject_area TEXT,
homepage TEXT,
contact_name TEXT,
contact_email TEXT,
description TEXT
);
COMMIT;");



$db->query("BEGIN;
        INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, region, meeting_type, subject_area, homepage, contact_name, contact_email, description)
	VALUES ('conference one', 'edit_key', '06/01/2010', '06/10/2010', 10, 'Atlanta', 'GA', 'summer school', 'algebraic topology', 'http://www.nilesjohnson.net', 'Linus', 'test@demo.com', 'This is going to be a summer school.');
        INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, region, meeting_type, subject_area, homepage, contact_name, contact_email, description)
	VALUES ('conference two', 'edit_key', '02/01/2010', '02/15/2010', 15, 'Chicago', 'IL', 'conference', 'algebraic topology', 'http://www.nilesjohnson.net', 'Justin', 'test@demo.com', 'This is going to be another great conference.');
        INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, region, meeting_type, subject_area, homepage, contact_name, contact_email, description) 
	VALUES ('conference three', 'edit_key', '03/01/2010', '03/15/2010', 15, 'Cody', 'WY', 'conference', 'algebraic topology', 'http://www.nilesjohnson.net', 'Seth', 'test@demo.com', 'This is going to be a mediocre conference.');
        COMMIT;");



$db->query("BEGIN;
INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, region, meeting_type, subject_area, homepage, contact_name, contact_email, description)
VALUES ('conference one', 'edit_key', '01/01/2010', '01/15/2010', 15, 'Athens', 'GA', 'conference', 'algebraic topology', 'http://www.nilesjohnson.net', 'Niles', 'test@demo.com', 'This is going to be a great conference.');
COMMIT;");


// print entries in the conferences table:   
$result = $db->query("SELECT * FROM conferences");
// iterate through the retrieved rows
while ($result->valid()) {
    // fetch current row
    $row = $result->current();     
    print_r($row);
// proceed to next row
    $result->next();
}

/*
*/

// not generally needed as PHP will destroy the connection
unset($db);

?> 

