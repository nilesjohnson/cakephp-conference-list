<?php 
 Print "Hello, World!<br>\n";
 ?> 

<?php


// create new database (OO interface)
//$dbnew = new SQLiteDatabase("algtop-conf/atc2.sqlite");
//$dbold = new SQLiteDatabase("algtop-conf/app/webroot/db-backup_2011-08-03.sqlite");


/*
$dbnew->query("BEGIN;
DROP TABLE conferences;
COMMIT;");

/*
*/

/*
$dbnew->query("BEGIN;
CREATE TABLE conferences (
id INTEGER PRIMARY KEY,
edit_key VARCHAR(10),
title VARCHAR(255),
start_date DATE,
end_date DATE,
duration NUMERIC DEFAULT NULL,
institution VARCHAR(255),
city VARCHAR(60),
country VARCHAR(60),
meeting_type VARCHAR(60),
subject_area VARCHAR(255),
homepage VARCHAR(255),
contact_name VARCHAR(255),
contact_email VARCHAR(255),
description TEXT
);
COMMIT;");

/**/

/*
$dbold->query("BEGIN;
        INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, country, meeting_type, subject_area, homepage, contact_name, contact_email, description)
	VALUES ('conference one', 'edit_key', '06/01/2009', '06/10/2009', 10, 'Atlanta', 'US', 'summer school', 'algebraic topology', 'http://www.nilesjohnson.net', 'Linus', 'test@demo.com', 'This is going to be a summer school.');
        INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, country, meeting_type, subject_area, homepage, contact_name, contact_email, description)
	VALUES ('conference two', 'edit_key', '02/01/2009', '02/15/2009', 15, 'Chicago', 'Germany', 'conference', 'algebraic topology', 'http://www.nilesjohnson.net', 'Justin', 'test@demo.com', 'This is going to be another great conference.');
        COMMIT;");

/**/



/*
// migrate the data    
$result = $dbold->query("SELECT * FROM conferences");
// iterate through the retrieved rows
while ($result->valid()) {
    // fetch current row
    $row = $result->current();   
    //print_r($row);

    $str = "('".
      sqlite_escape_string($row['title'])."', '".
      sqlite_escape_string($row['edit_key'])."', '".
      sqlite_escape_string($row['start_date'])."', '".
      sqlite_escape_string($row['end_date'])."', '".
      sqlite_escape_string($row['duration'])."', '".
      sqlite_escape_string($row['city'])."', '".
      sqlite_escape_string($row['country'])."', '".
      sqlite_escape_string($row['meeting_type'])."', '".
      sqlite_escape_string($row['subject_area'])."', '".
      sqlite_escape_string($row['homepage'])."', '".
      sqlite_escape_string($row['contact_name'])."', '".
      sqlite_escape_string($row['contact_email'])."', '".
      sqlite_escape_string($row['description'])."'".
      "); ";
    //print($str."<br>\n");
    print('<h4>'.$row['title']."</h4><br>\n");
    $qry = 'BEGIN; '.
      'INSERT INTO conferences (title, edit_key, start_date, end_date, duration, city, country, meeting_type, subject_area, homepage, contact_name, contact_email, description) VALUES '.
      $str .
      'COMMIT;';
    $dbnew->query($qry);
    //print($qry."<br>\n");
    
// proceed to next row
    $result->next();
}

/**/

/*
// print data   
$result = $dbnew->query("SELECT * FROM conferences");
// iterate through the retrieved rows
while ($result->valid()) {
    // fetch current row
    $row = $result->current();   
    print_r($row);
// proceed to next row
    $result->next();
}

/**/


// not generally needed as PHP will destroy the connection
unset($db);
/**/
?> 

