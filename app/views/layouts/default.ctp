<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AlgTop-Conf</title>
<!--
<title>AlgTop-Conf: <?php echo $title_for_layout?></title>
-->

<link href="http://www.nilesjohnson.net/favicon.ico" type="image/x-icon" rel="icon" />
<link rel="stylesheet" type="text/css"
href="/algtop-conf/css/cake.generic.css" />

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-11180825-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<?= $scripts_for_layout ?>
</head>

<body>

<div id="container">
  <div id="header">
    <h1><a href="http://nilesjohnson.net/algtop-conf/">AlgTop-Conf:</a>
    Conference Announcements</h1>
  </div>
  <div id="sub_header">
    <div id="menu">
      <?php echo $html->link('Main List',array('controller' =>
      'conferences', 'action' => 'index'))?>
      |
      <?php echo $html->link('Add Announcement',array('controller' =>
     'conferences', 'action' => 'add'))?>
      | 
      <?php echo $html->link('About',array('controller' =>
     'conferences', 'action' => 'about'))?>

      <div id="admin_contact">
	Trouble? Comments? <a href="http://www.nilesjohnson.net/contact.html">Contact Niles</a>
      </div>
    </div>
  </div>

  <div id="flashdiv">
    <?php echo $session->flash(); ?>
  </div>

  <!-- view content -->
  <div id="content">
    <?php echo $content_for_layout ?>
  </div>
  <!-- footer -->
  <div id="footer">
    <!-- footer content -->
    <div style="text-align: center"><a href="http://www.nilesjohnson.net">nilesjohnson.net</a></div>
  </div>
</div>


</body>
</html>
