<!-- File: /app/views/conferences/add.ctp -->	

<?php
$javascript->link(array(
'jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js', 
'jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js', 
), false);
$html->css("redmond/jquery-ui-1.8.10.custom.css", null, array("inline"=>false));
$javascript->codeBlock('
	$(function() {
		var dates = $( "#ConferenceStartDate, #ConferenceEndDate" ).datepicker({
                        dateFormat: "yy-mm-dd",
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "ConferenceStartDate" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
', array('inline'=>false));
?>

	
<h1>Add Meeting Information</h1>

<?php



echo $form->create('Conference');
echo $form->input('title');
echo $form->input('edit_key', array('type'=>'hidden'));
echo $form->input('start_date', array('type'=>'text', 'div'=>'input datefield', 'after'=>'yyyy-mm-dd'));
echo $form->input('end_date', array('type'=>'text', 'div'=>'input datefield', 'after'=>'yyyy-mm-dd'));
//echo $form->input('duration');
echo $form->input('city', array('label'=>'City and State/Province'));
echo $form->input('country', array( 'type'=>'select', 'options'=>$countries, 'default'=>'country'));
echo $form->input('homepage', array('label'=>'Conference website'));
echo $form->input('meeting_type', array('after'=>'e.g. conference, summer school, special session, etc.'));
echo $form->input('subject_area', array('after'=>'comma-separated list'));
echo $form->input('contact_name');
echo $form->input('contact_email', array('after'=>'never displayed publicly; confirmation and edit/delete codes will be sent to this address (or list of addresses)'));
echo $form->input('description', array('label'=>'Description: <br/><span style="font-size:80%;">Enter text, HTML, or <a href="http://daringfireball.net/projects/markdown/">Markdown</a>.</span>', 'rows' => '10'));
echo '<div class="input"><p>Description Preview:</p><div class="wmd-preview"></div></div>';
echo $form->input('captcha', array('label' => 'Please Enter the Sum of ' . $mathCaptcha, 'after'=>'anti-spam'));
echo $form->end('Submit');
?>




<script type="text/javascript">
		// to set WMD's options programatically, define a "wmd_options" object with whatever settings
		// you want to override.  Here are the defaults:
        wmd_options = {
			// format sent to the server.  Use "Markdown" to return the markdown source.
			output: "HTML",

			// line wrapping length for lists, blockquotes, etc.
			lineLength: 40,

			// toolbar buttons.  Undo and redo get appended automatically.
			buttons: "bold italic | link blockquote code | ol ul heading hr",

			// option to automatically add WMD to the first textarea found.  See apiExample.html for usage.
			autostart: true
		};
	</script>

<script type="text/javascript" src="/js/wmd/wmd.js"></script>
