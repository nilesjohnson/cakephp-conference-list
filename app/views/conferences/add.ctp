<!-- File: /app/views/conferences/add.ctp -->	

<?php
$javascript->link(array(
'jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js', 
'jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js', 
), false);
$html->css("redmond/jquery-ui-1.8.10.custom.css", null, array("inline"=>false));
$javascript->codeBlock('
	$(function() {
		var dates = $( "#ConferenceStartDate, #ConferenceEndDate" ).datepicker({
                        dateFormat: "yy-mm-dd",
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "ConferenceStartDate" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
', array('inline'=>false));

$javascript->codeBlock('
$(document).ready(function() {

  //Hide (Collapse) the toggle containers on load
  $(".togglebox").hide(); 

  //Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
  $("h4.trigger").click(function(){
    $(this).toggleClass("active").nextAll(".togglebox:first").slideToggle("fast");
    $(this).children("a:first").toggle();
    document.getElementById("CcDataFrom").value = document.getElementById("ConferenceContactEmail").value;
    document.getElementById("CcDataSubject").value = document.getElementById("ConferenceTitle").value;
    document.getElementById("CcDataBody").value = cc_body_prefill();
    return false; //Prevent the browser jump to the link anchor
  });

});
', array('inline'=>false));

$javascript->codeBlock('
function cc_add(strone,strtwo) {
    var initstr;
    var strmid = "@";
    var strend = ".edu";
    if (document.getElementById("CcDataTo").value == "") {
       initstr = "";
    }   
    else {
       initstr = ", ";
    }
    document.getElementById("CcDataTo").value += initstr+strone+strmid+strtwo+strend;
    return false;
}

function cc_body_prefill() {
    var body_text = "First announcement: \n\n";

    body_text += document.getElementById("ConferenceTitle").value+"\n";
    body_text += document.getElementById("ConferenceCity").value+", ";
    body_text += document.getElementById("ConferenceCountry").value+"\n";
    body_text += document.getElementById("ConferenceStartDate").value+" -- ";
    body_text += document.getElementById("ConferenceEndDate").value+"\n";
    body_text += document.getElementById("ConferenceHomepage").value+"\n\n";

    body_text += document.getElementById("ConferenceDescription").value+"\n\n";

    body_text += "For more details, see the website:\n";
    body_text += document.getElementById("ConferenceHomepage").value+"\n\n";

    body_text += "Hope to see you there!\n";
    body_text += document.getElementById("ConferenceContactName").value+"\n"+"on behalf of the organizers";
    return body_text;
}
', array('inline'=>false));

?>

	
<h1>Add Meeting Information</h1>

<?php



echo $form->create('Conference');
echo $form->input('title');
echo $form->input('edit_key', array('type'=>'hidden'));
echo $form->input('start_date', array('type'=>'text', 'div'=>'input datefield', 'after'=>'yyyy-mm-dd'));
echo $form->input('end_date', array('type'=>'text', 'div'=>'input datefield', 'after'=>'yyyy-mm-dd'));
//echo $form->input('duration');
echo $form->input('city', array('label'=>'City and State/Province'));
echo $form->input('country', array( 'type'=>'select', 'options'=>$countries, 'default'=>'country'));
echo $form->input('homepage', array('label'=>'Conference website'));
echo $form->input('institution', array('label'=>'Host institution', 'after'=>'University, institute, etc.'));
echo $form->input('meeting_type', array('after'=>'e.g. conference, summer school, special session, etc.'));
echo $form->input('subject_area', array('after'=>'comma-separated list'));
echo $form->input('contact_name');
echo $form->input('contact_email', array('after'=>'never displayed publicly; confirmation and edit/delete codes will be sent to this address (or list of addresses)'));
echo $form->input('description', array('label'=>'Description: <br/><span style="font-size:80%;">Enter text, HTML, or <a href="http://daringfireball.net/projects/markdown/">Markdown</a>.</span>', 'rows' => '10'));
echo '<div class="input"><p>Description Preview:</p><div class="wmd-preview"></div></div>';
?>


<div id="ccdatadiv">
<h4 id="ccdata-toggle" class="trigger" title="Show/Hide">
<span style="padding: 2pt; margin: 3pt; border: 1px solid #B3BBC7; background-color: #D3DBE7">
Copy to email lists... 
</span>
<a href="#" id="ccdata-pm">(+)</a></h4>
<div class="subtitle">
<p>Fill in addresses.  The email body will be prefilled with the data entered above, but you may edit the body before submitting.</p>
</div>
<hr style="margin: 0 5%;"/>
<div class="togglebox" id="ccdata">

<div class="subtitle"><p>Fill in the fields below and an email will be
sent to the specified addresses.  Additionally, a copy of the email
will be sent to the supplied <tt>From</tt> address.</p>

<p><span style="font-weight:bold">Please check carefully; the Subject
and Body will be sent <em>exactly</em> as they appear below</span>,
including any changes that you make now.  Note that the information
below is re-filled from the data above each time you show or hide this
section.  Details of how the information below is computed are given
on the <a
href="http://www.nilesjohnson.net/algtop-conf/conferences/about#ccdata_about"
target="blank">About Page</a> (opens in a new window). </p>

<p>If you decide not to send an email of this announcement, simply
leave the <tt>To</tt> field blank.  In that case, the data from this section
will be discarded.</p>

</div>


<?php 
echo $form->input('CcData.from');
echo $form->input('CcData.to', array('after'=>'Comma-separated list of addresses
<ul>
<li>&#187; Add <a href="#" onclick="cc_add(\'algtop-l\',\'lists.lehigh\'); return false;">ALGTOP-L</a> <br/>
&nbsp;&nbsp;&nbsp;&nbsp;Here is the <a href="https://lists.lehigh.edu/mailman/listinfo/algtop-l" target="blank">list information page</a>.</li>

<li>&#187; Add <a href="#" onclick="cc_add(\'GEOMETRY\',\'listserv.utk\'); return false;">GEOMETRY (UTK)</a> <br/>
&nbsp;&nbsp;&nbsp;&nbsp;Before posting, please read the <a href="http://nilesjohnson.net/geometry_list_instructions.html" target="blank">instructions</a>.</li>
</ul>
'));
?>

<?php
echo $form->input('CcData.subject');
echo $form->input('CcData.body', array('rows' => '15'));
?>
</div>
</div>

<?php
echo $form->input('captcha', array('label' => 'Please Enter the Sum of ' . $mathCaptcha, 'after'=>'anti-spam'));
echo $form->end('Submit');
?>

<script type="text/javascript">
		// to set WMD's options programatically, define a "wmd_options" object with whatever settings
		// you want to override.  Here are the defaults:
        wmd_options = {
			// format sent to the server.  Use "Markdown" to return the markdown source.
			output: "HTML",

			// line wrapping length for lists, blockquotes, etc.
			lineLength: 40,

			// toolbar buttons.  Undo and redo get appended automatically.
			buttons: "bold italic | link blockquote code | ol ul heading hr",

			// option to automatically add WMD to the first textarea found.  See apiExample.html for usage.
			autostart: true
		};
	</script>

<script type="text/javascript" src="/js/wmd/wmd.js"></script>
