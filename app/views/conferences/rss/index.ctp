
<?php
$this->set('documentData', array(
				 'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', array(
				'title' => __("AlgTop-Conf Announcements", true),
				'link' => $html->url('/', true),
				'description' => __("Upcoming Meetings.", true),
				'language' => 'en-us'));


foreach ($conferences as $conference) {

/*  
  $conferenceLink = array(
		    'controller' => 'entries',
		    'action' => 'view',
		    'year' => date('Y', $postTime),
		    'month' => date('m', $postTime),
		    'day' => date('d', $postTime),
		    $post['Post']['slug']);
*/
  // You should import Sanitize
  App::import('Sanitize');
  // This is the part where we clean the body text for output as the description 
  // of the rss item, this needs to have only text to make sure the feed validates
  $bodyText = $conference['Conference']['start_date']." <small>through</small> ".$conference['Conference']['end_date'];
  /*
  $bodyText = preg_replace('=\(.*?\)=is', '', $post['Post']['body']);
  $bodyText = $text->stripLinks($bodyText);
  $bodyText = Sanitize::stripAll($bodyText);
  $bodyText = $text->truncate($bodyText, 400, '...', true, true);
  */
  $bodyText .= ", ".$conference['Conference']['city']."; ".$conference['Conference']['country'];
  $bodyText = Sanitize::stripAll($bodyText);
  echo  $this->Rss->item(array(), array(
				  'title' => $conference['Conference']['title'],
				  'link' => $conference['Conference']['homepage'],
				  'guid' => array('url' => $conference['Conference']['homepage'], 'isPermaLink' => 'true'),
				  'description' =>  $bodyText,
				  'dc:creator' => '',
				  //'pubDate' => ''
					));
}

?>
