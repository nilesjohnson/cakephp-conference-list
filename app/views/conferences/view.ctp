<!-- File: /app/views/conferences/view.ctp -->

<div class="flash flash_bad">
Do you really want to delete the following announcement? This action cannot be undone.
</div>

<h2 class="title">
<?php echo '<a href="'.
   $conference['Conference']['homepage'].
   '">'.
   $conference['Conference']['title'].
   '</a>'
   ;?>
</h2>

<div class="conference">

<div class="dates">
   <?php echo $conference['Conference']['start_date']." <small>through</small> ".$conference['Conference']['end_date']; ?>
</div>

<?php
      if (!empty($conference['Conference']['institution'])) {
      	 echo "<div class=\"location\">";
      	 echo $conference['Conference']['institution'];
	 echo "</div>";
      }

?>

<div class="location">
<?php echo $conference['Conference']['city']."; ".$conference['Conference']['country'];?>
</div>


<!--<p>duration: <?php echo $conference['Conference']['duration']?></p>-->

<div class="conference_minor">
<p>Click title to visit website.</p>
<p>Meeting Type: <?php echo $conference['Conference']['meeting_type']?></p>
<p>Subject Area: <?php echo $conference['Conference']['subject_area']?></p>
<p>Contact: <?php echo $conference['Conference']['contact_name']?></p>
</div>

<h2>Description</h2>
<p class="description"><?php echo 
!$conference['Conference']['description'] ? 'none' : $conference['Conference']['description']
?></p>

<div class="flash flash_bad" style="margin-top: 2em;">
To delete, use the form below.
</div>

<?php
echo $form->create('Conference', array('action' => 'view'));
echo $form->input('captcha', array('label' => 'Please Enter the Sum of ' . $mathCaptcha));
echo $form->input('id', array('type'=>'hidden')); 
echo $form->input('edit_key', array('type'=>'hidden')); 
echo $form->end('Confirm Delete');
?>




