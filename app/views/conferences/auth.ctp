<!-- File: /app/views/conferences/auth.ctp -->

<h1>Please provde edit key</h1>
<?php
echo $form->create('Conference', array('action' => 'edit'));
echo $form->input('title');
echo $form->input('edit_key', array('type'=>'hidden'));
echo $form->input('start_date');
echo $form->input('end_date');
//echo $form->input('duration');
echo $form->input('city', array('label'=>'City and State'));
echo $form->input('country', array( 'type'=>'select', 'options'=>$countries, 'default'=>'country'));
echo $form->input('homepage', array('label'=>'Conference website'));
echo $form->input('meeting_type', array('after'=>'e.g. conference, summer school, special session, etc.'));
echo $form->input('subject_area', array('after'=>'comma-separated list'));
echo $form->input('contact_name');
echo $form->input('contact_email');
echo $form->input('description', array('label'=>'Description: use &lt;br&gt; for line breaks', 'rows' => '10'));
echo $form->input('captcha', array('label' => 'Please Enter the Sum of ' . $mathCaptcha, 'after'=> '(anti-spam)'));
echo $form->input('id', array('type'=>'hidden')); 
echo $form->end('Update Announcement');
?>
