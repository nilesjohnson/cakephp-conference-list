<!-- File: /app/views/conferences/index.ctp -->

<div class="intro_text">
<p>Welcome to the AlgTop-Conf List!  This is a home for conference announcements in algebraic topology and, more generally, meetings of any type which may be of any interest to the algebraic topology community.</p>

<p>There are a few other conference lists available, but this list aims to be more complete by allowing anyone at all to add announcements.  Rather than use a wiki, announcement information is stored in database format so that useful search functions can be added as the list grows.  Enjoy!
</p>
</div>

<hr/>
<h1><?php echo $view_title; ?></h1>


<div class="search_links">
<?php echo $sort_text ?>
<?php foreach ($search_links as $name => $array): ?>
<?php echo $html->link($name, $array)." "; ?>
<?php endforeach; ?>

<div style="float:right;">
<?php echo $html->link('Include Past',array('controller' =>
'conferences', 'action' => 'index', 'all'))?>
</div>
</div>


<?php foreach ($conferences as $conference): ?>

<h3 class="title">
<?php echo '<a href="'.
   $conference['Conference']['homepage'].
   '">'.
   $conference['Conference']['title'].
   '</a>'
   ;?>
</h3>
<div class="conference">

<div class="dates">
   <?php echo $conference['Conference']['start_date']." <small>through</small> ".$conference['Conference']['end_date']; ?>
</div>

<div class="location">
<?php echo $conference['Conference']['city']."; ".$conference['Conference']['country'];?>
</div>

<div class="action">
<a  id="description_<?php echo $conference['Conference']['id'];?>_plus" onclick="
   document.getElementById('description_<?php echo $conference['Conference']['id'];?>').style.display='block'; 
   document.getElementById('description_<?php echo $conference['Conference']['id'];?>_plus').style.display='none'; 
   document.getElementById('description_<?php echo $conference['Conference']['id'];?>_minus').style.display='inline'; 
   return false;" href="#">Description</a>
<a  id="description_<?php echo $conference['Conference']['id'];?>_minus" onclick="
   document.getElementById('description_<?php echo $conference['Conference']['id'];?>').style.display='none'; 
   document.getElementById('description_<?php echo $conference['Conference']['id'];?>_plus').style.display='inline'; 
   document.getElementById('description_<?php echo $conference['Conference']['id'];?>_minus').style.display='none'; 
   return false;" href="#" style="display:none;"> - Description</a>
 | 
<?php echo 
  $html->link('Report Problem', 
  array('action'=>'report', $conference['Conference']['id']));?>
<!--
 | 
<?php echo 
  $html->link('Edit', 
  array('action'=>'edit', $conference['Conference']['id'], 'key'));?>
 | 
<?php /* echo 
  $html->link('Delete', 
  array('controller' => 'conferences', 'action' => 'view', $conference['Conference']['id'], 'key') ); */?>
-->

</div>

<div class="conference_minor" id="description_<?php echo $conference['Conference']['id']?>">
<p>Meeting Type: <?php echo $conference['Conference']['meeting_type']?></p>
<p>Subject Area: <?php echo $conference['Conference']['subject_area']?></p>
<p>Contact: <?php echo 
!$conference['Conference']['contact_name'] ? 'see conference website' : $conference['Conference']['contact_name']?></p>
<!--
<p>Id: <?php /* echo $conference['Conference']['id'] */?></p>
<p>Edit Key: <?php /* echo $conference['Conference']['edit_key'] */?></p>
-->


<h3>Description</h3>
<p class="description"><?php echo 
!$conference['Conference']['description'] ? 'none' : $conference['Conference']['description']
?></p>
</div>





</div>

<?php endforeach; ?>
 
