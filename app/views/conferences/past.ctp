<!-- File: /app/views/conferences/index.ctp -->
<h1>All Conferences</h1>


<?php foreach ($conferences as $conference): ?>

<h3 class="title">
<?php echo '<a href="'.
   $conference['Conference']['homepage'].
   '">'.
   $conference['Conference']['title'].
   '</a>'
   ;?>
</h3>
<div class="conference">

<div class="dates">
   <?php echo $conference['Conference']['start_date']." <small>through</small> ".$conference['Conference']['end_date']; ?>
</div>

<div class="location">
<?php echo $conference['Conference']['city']."; ".$conference['Conference']['country'];?>
</div>

<div class="action">
<?php echo
  $html->link('Description',
  array('controller' => 'conferences', 'action' => 'view', $conference['Conference']['id'])); ?>
 | 
<?php echo 
  $html->link('Edit', 
  array('action'=>'edit', $conference['Conference']['id']));?>
 | 
<?php echo 
  $html->link('Delete', 
  array('controller' => 'conferences', 'action' => 'view', $conference['Conference']['id']));?>
</div>




</div>

<?php endforeach; ?>
 
