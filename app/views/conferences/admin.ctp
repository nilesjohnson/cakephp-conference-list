<!-- File: /app/views/conferences/view.ctp -->

<h1>Admin Page:</h1>
<h2 class="title">
<?php echo '<a href="'.
   $conference['Conference']['homepage'].
   '">'.
   $conference['Conference']['title'].
   '</a>'
   ;?>
</h2>

<div class="conference">

<div class="dates">
   <?php echo $conference['Conference']['start_date']." <small>through</small> ".$conference['Conference']['end_date']; ?>
</div>

<div class="location">
<?php echo $conference['Conference']['city']."; ".$conference['Conference']['country'];?>
</div>


<!--<p>duration: <?php echo $conference['Conference']['duration']?></p>-->

<div class="conference_minor">
<p>Click title to visit website.</p>
<p>Meeting Type: <?php echo $conference['Conference']['meeting_type']?></p>
<p>Subject Area: <?php echo $conference['Conference']['subject_area']?></p>
<p>Contact: <?php echo $conference['Conference']['contact_name']?></p>
</div>

<h2>Description</h2>
<p class="description"><?php echo 
!$conference['Conference']['description'] ? 'none' : $conference['Conference']['description']
?></p>

<!--
<div class="flash flash_bad" style="margin-top: 2em;">
Use the box below to recover the edit/delete keys for this announcement.
</div>
-->

<p style="padding:4ex 0;">
The edit key for this announcement is: 
<?php
echo $key_code;
?>
</p>



